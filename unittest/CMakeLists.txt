# Copyright (c) 2020-2022 Pawel Piskorz
# Licensed under the Eclipse Public License 2.0
# See attached LICENSE file

cmake_minimum_required(VERSION 3.15)
project(buffered_communication_ut CXX)


# install require conan packages
if(NOT EXISTS "${CMAKE_BINARY_DIR}/conan.cmake")
   message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
   file(DOWNLOAD "https://raw.githubusercontent.com/conan-io/cmake-conan/master/conan.cmake"
                 "${CMAKE_BINARY_DIR}/conan.cmake")
endif()

include(${CMAKE_BINARY_DIR}/conan.cmake)
conan_cmake_run(REQUIRES
    gtest/cci.20210126
    mock-fifo/0.0.0@zxvcv/testing
)

include(${CMAKE_BINARY_DIR}/unittest/conanbuildinfo.cmake)
conan_basic_setup()


# unit tests executable target
add_executable(buffered_communication_ut
    src/Buffered_Communication_UT.cpp
)

target_link_libraries(buffered_communication_ut PRIVATE
    buffered_communication
    ${CONAN_LIBS}
)

add_test(NAME buffered_communication_ut
    COMMAND buffered_communication_ut
)
