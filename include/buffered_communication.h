/* #################################################################################### *
 *                                LICENSE INFORMATIONS                                  *
 * ==================================================================================== *
 * Copyright (c) 2020-2022 Pawel Piskorz
 * Licensed under the Eclipse Public License 2.0
 * See attached LICENSE file
 * #################################################################################### */
/****************************************************************************************
 * NAME: Buffered_Communication
 *
 * ======================================================================================
 * COMMENTS:
 *
 * ======================================================================================
 * EXAMPLE:
 *
 ****************************************************************************************/

#ifndef ZXVCV_BUFFERED_COMMUNICATION_H_
#define ZXVCV_BUFFERED_COMMUNICATION_H_


/*************************************** INCLUDES ***************************************/
#include <stdbool.h>
#include "fifo.h"
#include "errors.h"


/************************************** DATA TYPES **************************************/
typedef struct BuffCommunication_Settings_Tag {
    Fifo_C* Buff_InputCommands;
    Fifo_C* Buff_IN;
    Fifo_C* Buff_OUT;

    // UART_HandleTypeDef* huart;

    uint8_t recieved;
    bool EOL_recieved;
    bool transmission;
} BuffCommunication_Settings;


/********************************* PUBLIC DECLARATIONS **********************************/
Std_Err init_buffered_communication(BuffCommunication_Settings* settings);//,
    // UART_HandleTypeDef* huart);

Std_Err send_buffered_message(BuffCommunication_Settings* settings);

Std_Err send_buffered_message_IT(BuffCommunication_Settings* settings);

Std_Err receive_buffered_message_IT(BuffCommunication_Settings* settings);

Std_Err add_message_to_send(BuffCommunication_Settings* settings, char* msg, uint8_t msgSize);

Std_Err deinit_buffered_communication(BuffCommunication_Settings* settings);

#endif /* ZXVCV_BUFFERED_COMMUNICATION_H_ */
