from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class MockFifoConan(ConanFile):
    name = "mock-fifo"
    version = "0.0.0"

    # Optional metadata
    license = "Eclipse Public License - v 2.0"
    author = "Pawel Piskorz ppiskorz0@gmail.com"
    url = "https://gitlab.com/zxvcv-conan/fifo"
    description = """ Mock for fifo queue component. """
    topics = ("mock", "queue", "fifo", "CPP")

    requires = "errors/[<1.0.0]@zxvcv/testing", "gtest/cci.20210126"

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "include/*"
    no_copy_source = True

    def export_sources(self):
        self.copy("*.h", dst="include", src="../include", keep_path=False)

    def package(self):
        self.copy("*.h")
