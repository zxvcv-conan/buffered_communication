# Copyright (c) 2020-2022 Pawel Piskorz
# Licensed under the Eclipse Public License 2.0
# See attached LICENSE file

cmake_minimum_required(VERSION 3.15)
project(mock-fifo CXX)


# create library
# add_library(${CMAKE_PROJECT_NAME}
#     src/mock_fifo.cpp
# )

target_include_directories(mock-fifo PUBLIC
    include
    ../include
)

set_target_properties(mock-fifo PROPERTIES
    PUBLIC_HEADER "include/mock_fifo.h"
)

install(TARGETS mock-fifo)
