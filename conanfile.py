from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class FifoConan(ConanFile):
    name = "buffered_communication"
    version = "0.0.1"

    # Optional metadata
    license = "Eclipse Public License - v 2.0"
    author = "Pawel Piskorz ppiskorz0@gmail.com"
    url = "https://gitlab.com/zxvcv-conan/fifo"
    description = """ Fifo queue component.
    Implementation for copy version and non-copy version.
    Could be used with interrupts (require use specific flag during compilation).
    """
    topics = ("buffered_communication", "communication", "C", "embedded")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False]
    }
    default_options = {
        "shared": False
    }
    requires = (
        "fifo/[<1.0.0]@zxvcv/testing",
        "errors/[<1.0.0]@zxvcv/testing"
    )

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "unittest/*"

    def configure(self):
        # TODO[PP]: if architecture if for embedded system, allow parameters 'interrupt_safe'
        #           configure settings and options ...

        # this is a C library, and does not depend on any C++ standard library
        del self.settings.compiler.libcxx
        del self.settings.compiler.cppstd

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.preprocessor_definitions
        tc.preprocessor_definitions["ZXVCV_FIFO_MODE_COPY_"] = True
        tc.preprocessor_definitions["ZXVCV_FIFO_MODE_NONCOPY_"] = True
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        cmake.test()
        # cmake.test(build_dir="cmake-build-debug")
        # cmake.test(args=['--', 'ARGS=-T Test'])

    def package(self):
        self.copy("LICENSE", dst="licenses", src=".")
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["buffered_communication"]

        # self.cpp_info.cflags = []  # pure C flags
        # self.cpp_info.cxxflags = []  # C++ compilation flags
        # self.cpp_info.sharedlinkflags = []  # linker flags
        # self.cpp_info.exelinkflags = []  # linker flags

    def imports(self):
        self.copy("*.h")
