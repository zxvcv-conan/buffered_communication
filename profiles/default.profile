[settings]
arch=x86_64
arch_build=x86_64
build_type=Debug
compiler=gcc
compiler.version=9
os=Linux

[options]
fifo:shared=True
fifo:mode=all
fifo:interrupts_safe=False

# [env]
# CC=/opt/rh/devtoolset-6/root/usr/bin/gcc
# CXX=/opt/rh/devtoolset-6/root/usr/bin/g++